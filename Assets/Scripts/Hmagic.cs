﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Hmagic : MonoBehaviour
{
    public Slider BarraM;
    public float Hpp = 5;
    // Start is called before the first frame update
    void Start()
    {
        Hpp = 5;
    }
    void OnTriggerEnter(Collider other)
    {


        if (other.gameObject.tag == "Magic1")
        {
            Hpp = Hpp - 1;
        }
        if (other.gameObject.tag == "Magic2")
        {
            Hpp = Hpp - 1;
        }
        if (other.gameObject.tag == "Magic3")
        {
            Hpp = Hpp - 1;
        }

    }

    // Update is called once per frame
    public void Update()
    {
        BarraM.value = Hpp;
        Hpp = Hpp - 0.000000000000000000000000001f;
        if (Hpp <= 2)
        {
            SceneManager.LoadScene("Game Win");
        }

    }
}
