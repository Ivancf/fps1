﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Botella : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {

            Destroy(this.gameObject);
        }

        if ((other.gameObject.tag == "Magic1") & (other.gameObject.tag == "Magic2") & (other.gameObject.tag == "Magic3"))
        {
            SceneManager.LoadScene("Game Win");
        }
    }
}