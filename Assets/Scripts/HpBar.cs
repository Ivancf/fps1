﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HpBar : MonoBehaviour
{
    public Slider BarraHp;
    public float Hp = 100;
    // Start is called before the first frame update
    void Start()
    {
        if (Hp <= 0)
        {
            SceneManager.LoadScene("Game Over");
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            Hp = Hp - 6;
        }
        if (other.gameObject.tag == "mas5HP")
        {
            Hp = Hp + 14;
        }
        if (other.gameObject.tag == "Cactus")
        {
            Hp = Hp - 3;
        }

        if ((other.gameObject.tag == "Magic1") & (other.gameObject.tag == "Magic2") & (other.gameObject.tag == "Magic3"))
        {
            SceneManager.LoadScene("Game Win");
        }

    }

    // Update is called once per frame
    public void Update()
    {
        BarraHp.value = Hp;
        Hp = Hp - 0.0057f;
        
        if (Hp <= 0)
        {
            SceneManager.LoadScene("Game Over");
        }

    }
}
